﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Xml.Linq;

namespace _03_xml
{
    class DataFetcher
    {

        /*
        * Ebben az esetben a futó programból lekérjük a típusokat
        * kiválasztjuk belőle ami nekünk kell (megfelelő attrib. alapján)
        * majd ennek a típusnak lekérjük a tulajdonságait és metódusait.
        * 
        * Ezt követően ezeket xml fájlba írjuk és elmentjük.
        * 
        * */

        public void FetchDataFromProgram()
        {
            // Jelenleg csak 1 elemre van megírva (FirstOrDefault), ami nem más mint a Dog objektum (vagy amire rárakódik az attrib.).

            ;
            ;
            ;

            
        }

        private void WriteToXML(Type objType, List<PropertyInfo> properties, List<MethodInfo> methods)
        {

            // 1. feladat:
            // Írjuk ki XML-be struktúráltan a következő formában:
            #region minta-xml
            /*
                <?xml version="1.0" encoding="utf-8"?>
                <entities>
                <entity>
                    <hash>1234610245</hash>
                    <type>Kutya</type>
                    <namespace>_03_xml</namespace>
                    <properties>
                        <property>System.String Nev</property>
                        <property>...</property>
                    </properties>
                    <methods>
                        <method>System.String get_Nev()</method>
                        <method>...</method>
                    </methods>
                </entity>
                </entities>
            */
            #endregion

            // 2. feladat:
            // módosítsuk a hash részt és helyezzük el attribútumként az entity tag-ben

            // 3. feladat
            // a properties és methods tag-ekben attribútumként jelenjen meg az elemek száma

            // 4. feladat
            // methods-ok esetén a () jeleket töröljük és úgy mentsük (erre Func-ot használjunk)

            // 5. feladat {HF}
            // methods-ok esetén legyen külön tag a visszatérésnek és külön a névnek

            // 6. feladat {HF}
            // ellenőrizzétek le, hogy mi történik ha az Entities.cs-ben a Kutya osztály helyett
            // az Auto osztályra teszitek rá a [ModelToXML] attribútumot
            // a teljes kimeneti xml ennek megfelelően frissülni fog
            
            // 7. feladat {HF}
            // csináljátok meg a FetchDataFromProgram metódust úgy, hogy több db entitásra is működjön
            // amin rajta van a [ModelToXML] attribútum

            ;
            ;
            ;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            DataFetcher df = new DataFetcher();
            df.FetchDataFromProgram();
        }
    }
}
