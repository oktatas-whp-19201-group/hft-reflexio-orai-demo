﻿using System;
using System.Linq;
using System.Reflection;

namespace _02_reflection
{
    // osztályok létrehozása ide (+ később attribútumok)
    interface IMyObject 
    {
        // szándékosan üres
    }

    class Person : IMyObject
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }

    class Student : Person
    {
        public string Email { get; set; }

        public string NeptunID { get; set; }

        public int Credits { get; set; }
    }

    class BscStudent : Student
    {
        public int EnrollmentYear; // sima mező, nem tulajdonság

        private int ActiveSemesters_1; // priv. nem látszik
        public int ActiveSemesters_2;
        public int ActiveSemesters_3;
        public int ActiveSemesters_4;

        public string Greeting()
        {
            return "Hi! I'm "+ Name +", nice to meet you!";
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            # region 01-BASICS-DEMO

            // typeof vs GetType
            // GetProperties / Method / Fields

            #endregion

            
            
            
            // ##############################################################################################################################




            # region 02-EXAMPLE-GENERATE-RANDOM-TYPES

            // generáljuk random típusokat az osztályok mentén
            // reflexióval írjuk ki típusonként a metódusokat/tulajdonságokat/adattagokat

            #endregion
            



            // ##############################################################################################################################




            #region 03-EXAMPLE-WITH-ATTRIBUTES

            // 0. RÉSZ
            // Hozzuk létre a szükséges attribútumot.
            // - CheckLength: int MaxLength tulajdonsággal állítható
            // Alkalmazzuk ezt az Email tulajdonságra Student-ben.

            // 1. RÉSZ
            // A Student típusok emailjeit ellenőrizzük le! (pl. a user beír valamit, és még mielőtt elmentenénk)
            // Reflexió segítségével vizsgáljuk meg a teljes tömböt, HA Student típusról van szó
            // akkor szűrjünk a megfelelő tulajdonságra és attribútum felhasználásával ellenőrizzük
            // annak helyességét.

            
            ;
            ;
            ;


            // HF.: nem csak karakterszámra nézzük az emailt, hanem pl. van-e benne @ jel.
            // Ehhez itt található egy nagyon jó ValidationFactory példa. Érdemes átnézni és logikailag végigkövetni.
            // Ha 2 vagy több attribútum alapján akarunk validálni, akkor ez a helyes irány!
            // link: https://gitlab.com/siposm/oktatas-hft-20211/-/blob/master/_ARCHIVED/LA-04-reflexio/reflexio_feladat-1-VALIDATOR/reflexio_feladat/Program.cs

            // 2. RÉSZ
            // Ellenőrzés: kérjük le a Student típusokat és írjuk ki az email címüket.

            ;
            ;
            ;

            #endregion
        }
    }
}
